Creeaza docker-compose care porneste 3 instante:
    - Una de DB 
    - Una de Node.js
    - Una de Nginx.

I. Aplicatia de Node.js foloseste Hapi cu un plugin care expune MINIM 3 endpointuri.

    {Easy}   1 health check
    {Medium} 1 create new user/ register
    {Hard}   1 login    
    
II. Pentru instanta de DB

    {Easy}   Datele vor fi stocate in Mongo
        OR
    {Medium} Datele vor fi stocate in PostreSQL 
        OR
    {Hard}   Datele vor fi stocate in PostreSQL in format Jsonb.

III. Serverul de Nginx serveste static content. Acest content este un build a unei aplicatii de Vue. Aplicatia are urmatoarele reguli. 

    {Easy}   1. Se incarca doar daca ruta de healthcheck returneaza 200 OK
    {Easy}   2. Poti ajunge pe o pagina X doar daca ai trecut de Login
    {Medium} 3. Decizia pe ce pagina poti intra (X, Y, Z) o faci pe baza unui key "role" din JWT.
    {Medium} 4. Un user (cu "rolul": "internal"), poate creea alti useri cu "rolul" : "external". User cu "rolul": "internal" se creeaza la register. 
    {Medium} 5. Un user cu "rolul": "internal", poate vedea toti userii cu "rolul" : "external" si poate sterge acei useri.
	{Hard}   6. Vizionarea si creearea userilor trebuie sa fie pe aceeasi pagina si sa fie real-time. Pentru aceasta foloseste Vuex.
	{Bonus}  7. Pentru fiecare "external" user creeat, se cauta pe Flickr o poza cu hashtag-ul "username" (ex. user: John -> #john). Se alege din lista returnata se alege prima si se salveaza in DB la creere.



Important la aceast test este sa folosesti git. Dorim sa vedem fiecare commit individuaL si prin asta, parcursul si abordarea in solutionarea problemei.

Success.