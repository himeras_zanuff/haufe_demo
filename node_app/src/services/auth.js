module.exports = [
    {
        method: 'GET',
        path: '/register_user({uname},{pw})',
        handler: async function (request, h) {
            let select = `SELECT verify_credentials('${request.params.uname}','${request.params.pw}')`;
            try {
                const result = await request.pg.client.query(select);
                console.log(result);
                return h.response(result.rows[0]);
            } catch (err) {
                console.log(err);
            }
        }
    },
    {
        method: 'GET',
        path: '/verify_user({uname},{pw})',
        handler: async function (request, h) {
            let select = `SELECT verify_credentials('${request.params.uname}','${request.params.pw}')`;
            try {
                const result = await request.pg.client.query(select);
                console.log(result);
                return h.response(result.rows[0]);
            } catch (err) {
                console.log(err);
            }
        }
    },
    {
        method: 'GET',
        path: '/users',
        handler: async function (request, h) {
            let select = `SELECT * FROM users`;

            try {
                const result = await request.pg.client.query(select);
                console.log(result);
                return h.response(result.rows[0]);
            } catch (err) {
                console.log(err);
            }
        }
    }
];