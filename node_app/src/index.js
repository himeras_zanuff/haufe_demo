'use strict';

const Hapi = require('@hapi/hapi');
const HapiPostgresConnection = require('hapi-postgres-connection');
const routes = require('./config/routes');
const init = async () => {
    const server = Hapi.server({
        port: 5555,
        host: '0.0.0.0'
    });
    await server.register({
        plugin: HapiPostgresConnection
    });
    server.route(routes);

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();